#coding=utf-8  
#
import sys
from PyQt4 import QtGui,QtCore,uic
from urllib import request
from urllib import parse

import zlib
from xml.dom.minidom import parseString

myw = uic.loadUiType("wnltq.ui")[0]




class mywindows(QtGui.QMainWindow,myw):
    def __init__(self,parent=None):
        # super(self).__init__(self,parent)
        QtGui.QMainWindow.__init__(self,parent)
        self.setupUi(self)
        self.pushButton.clicked.connect(self.slotExtension)
        self.pushButton_2.clicked.connect(self.getweatherinfo)
        self.lineEdit.editingFinished.connect(self.getweatherinfo)
        self.HorizontalHeaderlist=[u'昨天',u'今天','1','2','3','4']
        # self.groupBox.hide()
    def getweatherinfo(self):
        url = "http://wthrcdn.etouch.cn/WeatherApi?city="
        con = request.urlopen(url+parse.quote(self.lineEdit.text())).read()
        uz = zlib.decompress(con,16+zlib.MAX_WBITS).decode()
        dom = parseString(uz)
        root = dom.documentElement

        error_node = root.getElementsByTagName("error")
        if len(error_node):
            QtGui.QMessageBox.information(self,"提示","请输入正确的城市名称！！！")
            # QtGui.QMessageBox.information(self, "提示", self.tr("请填写正确的中文城市名！！！"))

            return(0)


        self.lineEdit.setText(root.getElementsByTagName('city')[0].firstChild.data)
        self.label.setText(root.getElementsByTagName('wendu')[0].firstChild.data)
        # self.label.setText(root.getElementsByTagName('wendu')[0].childNodes[0].nodeValue)
        self.label_7.setText(root.getElementsByTagName('shidu')[0].firstChild.data)
        self.label_4.setText(root.getElementsByTagName('fengxiang')[0].firstChild.data+u' 日出：%s 日落：%s'%(
            root.getElementsByTagName('sunrise_1')[0].firstChild.data,
            root.getElementsByTagName('sunset_1')[0].firstChild.data))

        self.textBrowser.setText("N/A")
        MajorPollutants_node =  root.getElementsByTagName('MajorPollutants')
        if len(MajorPollutants_node):
            try:
                self.textBrowser.setText(u'主要污染物为'+MajorPollutants_node[0].childNodes[0].nodeValue)
            except:
                pass
        suggest_node = root.getElementsByTagName('suggest')
        if len(suggest_node):
            self.textBrowser.append(suggest_node[0].firstChild.data)

        hhl = []
        forecast_node =  root.getElementsByTagName('forecast')
        date_node = forecast_node[0].getElementsByTagName('date')
        for date in date_node:
            hhl.append(date.firstChild.data)
        for i in range(4):
            self.HorizontalHeaderlist[i+2]=hhl[i+1]
        self.tableWidget.setHorizontalHeaderLabels(self.HorizontalHeaderlist)

        yestoday_node = root.getElementsByTagName('yesterday')
        self.tableWidget.setItem(0, 0, QtGui.QTableWidgetItem(
            yestoday_node[0].getElementsByTagName('high_1')[0].firstChild.data.split()[1]))
        self.tableWidget.setItem(1, 0, QtGui.QTableWidgetItem(
            yestoday_node[0].getElementsByTagName('low_1')[0].firstChild.data.split()[1]))

        day_1_node = yestoday_node[0].getElementsByTagName('day_1')
        self.tableWidget.setItem(2, 0, QtGui.QTableWidgetItem(
            day_1_node[0].getElementsByTagName('type_1')[0].firstChild.data))
        self.tableWidget.setItem(3, 0, QtGui.QTableWidgetItem(
            day_1_node[0].getElementsByTagName('fx_1')[0].firstChild.data))
        self.tableWidget.setItem(4, 0, QtGui.QTableWidgetItem(
            day_1_node[0].getElementsByTagName('fl_1')[0].firstChild.data))

        night_1_node = yestoday_node[0].getElementsByTagName('night_1')
        self.tableWidget.setItem(5, 0, QtGui.QTableWidgetItem(
            night_1_node[0].getElementsByTagName('type_1')[0].firstChild.data))
        self.tableWidget.setItem(6, 0, QtGui.QTableWidgetItem(
            night_1_node[0].getElementsByTagName('fx_1')[0].firstChild.data))
        self.tableWidget.setItem(7, 0, QtGui.QTableWidgetItem(
            night_1_node[0].getElementsByTagName('fl_1')[0].firstChild.data))

        forecast_node = root.getElementsByTagName('forecast')

        weather_node_l = forecast_node[0].getElementsByTagName('weather')
        for weather_node, i in zip(weather_node_l, range(5)):
            self.tableWidget.setItem(0, i+1, QtGui.QTableWidgetItem(
                weather_node.getElementsByTagName('high')[0].firstChild.data.split()[1]))
            self.tableWidget.setItem(1, i + 1, QtGui.QTableWidgetItem(
                weather_node.getElementsByTagName('low')[0].firstChild.data.split()[1]))

            day_node = weather_node.getElementsByTagName('day')
            self.tableWidget.setItem(2, i + 1, QtGui.QTableWidgetItem(
                day_node[0].getElementsByTagName('type')[0].firstChild.data))
            self.tableWidget.setItem(3, i + 1, QtGui.QTableWidgetItem(
                day_node[0].getElementsByTagName('fengxiang')[0].firstChild.data))
            self.tableWidget.setItem(4, i + 1, QtGui.QTableWidgetItem(
                day_node[0].getElementsByTagName('fengli')[0].firstChild.data))

            night_node = weather_node.getElementsByTagName('night')
            self.tableWidget.setItem(5, i + 1, QtGui.QTableWidgetItem(
                night_node[0].getElementsByTagName('type')[0].firstChild.data))
            self.tableWidget.setItem(6, i + 1, QtGui.QTableWidgetItem(
                night_node[0].getElementsByTagName('fengxiang')[0].firstChild.data))
            self.tableWidget.setItem(7, i + 1, QtGui.QTableWidgetItem(
                night_node[0].getElementsByTagName('fengli')[0].firstChild.data))

        environment_node = root.getElementsByTagName('environment')
        if len(environment_node):
            pm25 = environment_node[0].getElementsByTagName('pm25')[0].firstChild.data
            self.label_8.setText(pm25)
            self.label_24.setText(pm25)
            self.label_10.setText(environment_node[0].getElementsByTagName('aqi')[0].firstChild.data\
                                    +environment_node[0].getElementsByTagName('quality')[0].firstChild.data)
            self.label_22.setText(environment_node[0].getElementsByTagName('pm10')[0].firstChild.data)
            self.label_21.setText(environment_node[0].getElementsByTagName('o3')[0].firstChild.data)
            self.label_20.setText(environment_node[0].getElementsByTagName('co')[0].firstChild.data)
            self.label_23.setText(environment_node[0].getElementsByTagName('so2')[0].firstChild.data)
            self.label_19.setText(environment_node[0].getElementsByTagName('no2')[0].firstChild.data)
        else:
            self.label_8.setText("n/a")
            self.label_24.setText("n/a")
            self.label_10.setText("n/a")
            self.label_22.setText("n/a")
            self.label_21.setText("n/a")
            self.label_20.setText("n/a")
            self.label_23.setText("n/a")
            self.label_19.setText("n/a")

        zhishus_node = root.getElementsByTagName("zhishus")
        zhishu_node_l = zhishus_node[0].getElementsByTagName("zhishu")
        self.label_26.setText(zhishu_node_l[0].getElementsByTagName("value")[0].firstChild.data)
        self.label_25.setToolTip("\n".join(zhishu_node_l[0].getElementsByTagName("detail")[0].firstChild.data.split(u"，")).replace(u"。",u"\n"))

        self.label_28.setText(zhishu_node_l[1].getElementsByTagName("value")[0].firstChild.data)
        self.label_27.setToolTip("\n".join(zhishu_node_l[1].getElementsByTagName("detail")[0].firstChild.data.split(u"，")).replace(u"。",u"\n"))

        self.label_30.setText(zhishu_node_l[2].getElementsByTagName("value")[0].firstChild.data)
        self.label_29.setToolTip(
            "\n".join(zhishu_node_l[2].getElementsByTagName("detail")[0].firstChild.data.split(u"，")).replace(u"。",
                                                                                                              u"\n"))
        self.label_32.setText(zhishu_node_l[3].getElementsByTagName("value")[0].firstChild.data)
        self.label_31.setToolTip(
            "\n".join(zhishu_node_l[3].getElementsByTagName("detail")[0].firstChild.data.split(u"，")).replace(u"。",
                                                                                                              u"\n"))
        self.label_34.setText(zhishu_node_l[4].getElementsByTagName("value")[0].firstChild.data)
        self.label_33.setToolTip(
            "\n".join(zhishu_node_l[4].getElementsByTagName("detail")[0].firstChild.data.split(u"，")).replace(u"。",
                                                                                                              u"\n"))
        self.label_40.setText(zhishu_node_l[5].getElementsByTagName("value")[0].firstChild.data)
        self.label_39.setToolTip(
            "\n".join(zhishu_node_l[5].getElementsByTagName("detail")[0].firstChild.data.split(u"，")).replace(u"。",
                                                                                                              u"\n"))
        self.label_38.setText(zhishu_node_l[6].getElementsByTagName("value")[0].firstChild.data)
        self.label_37.setToolTip(
            "\n".join(zhishu_node_l[6].getElementsByTagName("detail")[0].firstChild.data.split(u"，")).replace(u"。",
                                                                                                              u"\n"))
        self.label_36.setText(zhishu_node_l[7].getElementsByTagName("value")[0].firstChild.data)
        self.label_35.setToolTip(
            "\n".join(zhishu_node_l[7].getElementsByTagName("detail")[0].firstChild.data.split(u"，")).replace(u"。",
                                                                                                              u"\n"))
        self.label_42.setText(zhishu_node_l[8].getElementsByTagName("value")[0].firstChild.data)
        self.label_41.setToolTip(
            "\n".join(zhishu_node_l[8].getElementsByTagName("detail")[0].firstChild.data.split(u"，")).replace(u"。",
                                                                                                              u"\n"))
        self.label_48.setText(zhishu_node_l[9].getElementsByTagName("value")[0].firstChild.data)
        self.label_47.setToolTip(
            "\n".join(zhishu_node_l[9].getElementsByTagName("detail")[0].firstChild.data.split(u"，")).replace(u"。",
                                                                                                              u"\n"))
        self.label_46.setText(zhishu_node_l[10].getElementsByTagName("value")[0].firstChild.data)
        self.label_45.setToolTip(
            "\n".join(zhishu_node_l[10].getElementsByTagName("detail")[0].firstChild.data.split(u"，")).replace(u"。",
                                                                                                              u"\n"))

    def slotExtension(self):
        if self.groupBox.isHidden():
            self.groupBox.show()
            self.pushButton.setText(u"详细<<<")
        else:
            self.groupBox.hide()
            self.pushButton.setText(u"详细>>>")


app = QtGui.QApplication(sys.argv)
w = mywindows()
w.show()
app.exec_()




# url = "http://wthrcdn.etouch.cn/WeatherApi?citykey=101090101"

# con = request.urlopen(url).read()
#
# uz = zlib.decompress(con,16+zlib.MAX_WBITS).decode()
#
# dom = parse('view-source_wthrcdn.etouch.cn_WeatherApi_citykey=101090101.xml')
# root = dom.documentElement
#
# a = root.getElementsByTagName("shidu")
# print(a)
# print(a[0].firstChild.data)
# print("nodeName:",root.nodeName)
# print("nodeValue:",root.nodeValue)
# print("nodeType:",root.nodeType)
# print("ElEMENT_NODE",root.ELEMENT_NODE)

# bb = root.getElementsByTagName("city")
# print(bb)
# b=bb[0]
# print(b.nodeName)
# print(b.nodeValue)
# print(b.firstChild.data)

# environment = root.getElementsByTagName("environment")
# pm25 = environment[0]
# print(pm25.nodeName)
# print(pm25.nodeValue)
# # print(pm25.firstChild.data)
# print(type(root))
# print(type(environment))

# pm25l = root.getElementsByTagName("pm25")
# pm25 = pm25l[0]
# print(pm25)
# print(pm25.firstChild.data)
#
# weather_l = root.getElementsByTagName("weather")
# # for weather in weather_l:
# for weather in weather_l:
#     print(weather.getElementsByTagName("date")[0].childNodes[0].data)
#     print(weather.getElementsByTagName("high")[0].childNodes[0].data)
#     print(weather.getElementsByTagName("low")[0].childNodes[0].data)
#     print(u'白天：')
#     day=weather.getElementsByTagName("day")
#     for dl in day:
#         print(dl.getElementsByTagName("type")[0].childNodes[0].data)
#         print(dl.getElementsByTagName("fengxiang")[0].childNodes[0].data)
#         print(dl.getElementsByTagName("fengli")[0].childNodes[0].data)
#     print(u'晚上：')
#     night=weather.getElementsByTagName("night")
#     for nl in night:
#         print(nl.getElementsByTagName("type")[0].childNodes[0].data)
#         print(nl.getElementsByTagName("fengxiang")[0].childNodes[0].data)
#         print(nl.getElementsByTagName("fengli")[0].childNodes[0].data)
